#!/bin/bash

# Basic information
export romsite="miui.am" # ROM name

# Translation variables
export maindevice="gemini" # Main device to extract translated XMLs from
export mainXML="MIUIAM-xml-main" # Repository name to store XMLs on
export mainLOCALE="Global" # Main language

# Crowdin variables
export crowdinPROJECT="miuilite" # Crowdin project name
export crowdinAPIKEY="f28ce8f21a8c398d01df52899630c651" # Crowdin API KEY

# Directories
export kdir="/miuibrasil/kitchen" # Kitchen directory
export cdir="${kdir}/translator" # Kitchen compiler directory
export cinput="${cdir}/input" # Kitchen compiler input for ROM files
export dinput="${kdir}/input/deodex" # Kitchen input for odexed ROMs
export deoinput="${kdir}/input" # Kitchen input for deodexed ROMs
export compout="${kdir}/output" # Kitchen output for compiled ROMs
export coutput="${cdir}/output" # Kitchen compiler output
export kerneldir="${kdir}/bin/AIK" # Kitchen kernel decompiler and compiler directory
export workspace="${kdir}/workspace" # Kitchen workspace

# Environment variables
export PATH=${PATH}:${kdir}/bin:${kdir}/bin/deodexerno:${kdir}/bin/AIK:${kdir}/bin/conversor

# Jars
export deodexer="java -jar ${kdir}/bin/Deodexer/Launcher.jar"
export translator="java -jar ${kdir}/translator/jbart3h.jar"
