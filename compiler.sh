#!/bin/bash
# New MIUI.AM Kitchen
# Created by Matheus Ferreira

# Checks if user is root. If not, enable usage of sudo to handle restricted requests.
if [[ "$(whoami)" == "root" ]]; then
  usesudo=""; export usesudo
else
  usesudo="sudo "; export usesudo
fi

# Imports basic configuration files to work with the compiler
source config.sh

# Cleanup
cleanup(){
  ${usesudo}chown -R "$(whoami)" ${kdir}
  ${usesudo}rm -rf ${kdir}/.cache/* ${dinput}/thetmpdir
  ${usesudo}rm -rf ${cdir}/projects/* ${cdir}/input/* ${cdir}/output/*
  ${usesudo}rm -rf ${kdir}/bin/LODTRTA/source/*
  ${usesudo}rm -rf ${kdir}/build.prop
  mkdir -p ${kdir}/XMLs ${kdir}/.cache/ROM/system ${kdir}/.cache/ROM/apk_wip
  ${usesudo}touch ${deoinput}/.placeholder ${compout}/.placeholder ${dinput}/.placeholder ${cdir}/input/.placeholder ${cdir}/output/.placeholder ${cdir}/projects/.placeholder
  ${usesudo}touch ${kdir}/.cache/.placeholder ${kdir}/XMLs/.placeholder ${cdir}/logs/.placeholder ${kdir}/.cache/.placeholder ${kdir}/XMLs/.placeholder
}

# Header of the menus
header() {
  clear
  echo "======================================"
  echo "= MIUI.AM ROM Compiler - $(date +%d/%m/%Y) ="
  echo "= V10.5.19 - Website: http://miui.am ="
  echo "======================================"
  echo ""
}

# Footer of the menus
footer () {
  echo ""
  echo "======================================"
}

# Main menu
mainmenu() {
  header
  echo " 1. Download ROMs"
  echo " 2. Deodex ROMs"
  echo " 3. Extract Source XMLs"
  echo " 4. Extract Global XMLs"
  echo " 5. Create patchable APK pack"
  echo " 6. Manage repositories"
  echo " 7. Download translations"
  echo " 8. Compile ROMs"
  echo " 9. Upload ROMs"
  echo " 0. Exit system"
  footer
  echo -n "Option: "; read -r choice
  case ${choice} in
    1) download_roms; unset choice; mainmenu;;
    2) deodex_roms; unset choice; mainmenu;;
    3) extract_source; unset choice; mainmenu;;
    4) extract_global; unset choice; mainmenu;;
    5) create_pack; unset choice; mainmenu;;
    6) manage_repos; unset choice; mainmenu;;
    7) download_trans; unset choice; mainmenu;;
    8) compile_roms; unset choice; mainmenu;;
    9) upload_roms; unset choice; mainmenu;;
    0) sleep 1; exit;;
  esac
}

# Download ROMS
download_roms() {
  header
  echo "ROM Downloader"
  echo "-"
  echo "Please insert all URLs on the next screen."
  footer
  sleep 2
  cd ${dinput} || exit
  nano romlist
  for romurl in $(<romlist); do
    echo ""; echo "Downloading ${romurl}..."
    axel -a -n5 ${romurl}
  done
  sleep 2
  cd ${kdir} || exit
}

# ROM deodexer
deodex_roms() {
  cd ${dinput} || exit
  for romfile in *.zip; do
    cleanup; unset romdevice; unset romversion; unset rombase
    romdevice="$(echo ${romfile} | cut -d '_' -f2)"; export romdevice
    romversion="$(echo ${romfile} | cut -d '_' -f3)"; export romversion
    rombase="$(echo ${romfile} | cut -d '_' -f5 | cut -c1-3)"; export rombase
    if [[ "$(unzip -v ${romfile} | grep build.prop)" == "" ]]; then
      if [[ ${rombase} == "8.0" ]] || [[ ${rombase} == "8.1" ]]; then
        echo "Deodexing/Converting Oreo ROM ${romfile}..."
        convertdex ${dinput}/${romfile}
        mv output.zip ${deoinput}/deodexed/miui_${romdevice}_${romversion}_deodexed_${rombase}.zip
      else
        echo "Converting ROM ${romfile}..."
        convertrom ${dinput}/${romfile}
        mv output.zip ${deoinput}/miui_${romdevice}_${romversion}_converted_${rombase}.zip
      fi
      mv ${dinput}/${romfile} ${dinput}/done
    else
      cp ${dinput}/${romfile} ${deoinput}/miui_${romdevice}_${romversion}_converted_${rombase}.zip
      mv ${dinput}/${romfile} ${dinput}/done
    fi
    if [[ ${rombase} == "4.4" ]] || [[ ${rombase} == "5.0" ]] || [[ ${rombase} == "5.1" ]] || [[ ${rombase} == "6.0" ]]; then
      echo "Deodexing ${romfile}..."
      unzip -q ${deoinput}/miui_${romdevice}_${romversion}_converted_${rombase}.zip -d ${kdir}/bin/LODTRTA/source
      xvfb-run java -jar ${kdir}/bin/LODTRTA/Launcher.jar ${kdir}/bin/LODTRTA/source/system z
      cd ${kdir}/bin/LODTRTA/source || exit
      zip -qr output.zip *
      mv output.zip ${deoinput}/deodexed/miui_${romdevice}_${romversion}_deodexed_${rombase}.zip
      rm -rf ${deoinput}/miui_${romdevice}_${romversion}_converted_${rombase}.zip
      cd ${dinput} || exit
      rm -rf ${kdir}/bin/LODTRTA/source
    fi
    if [[ ${rombase} == "7.0" ]] || [[ ${rombase} == "7.1" ]]; then
      echo "Deodexing ${romfile}..."
      cp ${deoinput}/miui_${romdevice}_${romversion}_converted_${rombase}.zip ${cinput}/input.zip
      xvfb-run java -jar ${kdir}/translator/jbart3h.jar deodex -api 0 -outputdir ${coutput} -romfile ${cinput}/input.zip -workingdir ${cdir}/projects
      if ls ${coutput}/deodexed_input.zip 1> /dev/null 2>&1; then
        mv ${coutput}/deodexed_input.zip ${deoinput}/deodexed/miui_${romdevice}_${romversion}_deodexed_${rombase}.zip
      else
        mv ${cinput}/input.zip ${deoinput}/deodexed/miui_${romdevice}_${romversion}_deodexed_${rombase}.zip
      fi
      rm -rf ${deoinput}/miui_${romdevice}_${romversion}_converted_${rombase}.zip
    fi
    cd ${dinput} || exit
  done
  echo "All done. Deodexed ROMs located in ${deoinput}/deodexed."
  echo "Original ROMs moved to ${dinput}/done"
  sleep 2
}

# Extract source
extract_source() {
  cd ${deoinput}/deodexed || exit
  for romfile in *.zip; do
    echo ""; echo "Extracting XMLs from ${romfile}..."; echo ""
    cleanup
    unzip -qj ${romfile} *.apk *build.prop -d ${kdir}/.cache/ROM/system
    cd ${kdir}/.cache/ROM || exit
    devname="$(cat system/build.prop | grep ro.product.device= | cut -d "=" -f2)"; export devname
    echo "Installing device frameworks..."
    apktool if ${kdir}/.cache/ROM/system/framework-res.apk > /dev/null 2>&1
    apktool if ${kdir}/.cache/ROM/system/framework-ext-res.apk > /dev/null 2>&1
    apktool if ${kdir}/.cache/ROM/system/miui.apk > /dev/null 2>&1
    apktool if ${kdir}/.cache/ROM/system/miuisystem.apk > /dev/null 2>&1
    apktool if ${kdir}/.cache/ROM/system/PersonalAssistantPlugin.apk > /dev/null 2>&1
    for apk in $(cat ${cdir}/devices/${devname}); do
      apk_name="$(basename ${apk})"; export apk_name
      if [[ -e ${kdir}/.cache/ROM/system/${apk} ]]; then
        rm -rf ${kdir}/.cache/apk_wip
        echo ""; echo "Decoding ${apk_name} XMLs..."
        apktool decode -f -s ${kdir}/.cache/ROM/system/${apk} -o ${kdir}/.cache/apk_wip > /dev/null 2>&1
        for xmltype in $(cat ${cdir}/devices/xmltypes); do
          if [[ -e ${kdir}/.cache/apk_wip/res/values/${xmltype}.xml ]]; then
            sed -i '/APKTOOL_DUMMY/d' ${kdir}/.cache/apk_wip/res/values/${xmltype}.xml
            if [[ ${devname} == ${maindevice} ]]; then
              mkdir -p ${kdir}/XMLs/Source/main/${apk_name}/res/values
              cp ${kdir}/.cache/apk_wip/res/values/${xmltype}.xml ${kdir}/XMLs/Source/main/${apk_name}/res/values
            else
              mkdir -p ${kdir}/XMLs/Source/device/${devname}/${apk_name}/res/values
              cp ${kdir}/.cache/apk_wip/res/values/${xmltype}.xml ${kdir}/XMLs/Source/device/${devname}/${apk_name}/res/values
            fi
          fi
        done
      fi
      cd ${deoinput}/deodexed || exit
    done
  done
}

# Extract global XMLs
extract_global() {
  cd ${deoinput}/deodexed || exit
  for romfile in *.zip; do
    echo ""; echo "Extracting Global XMLs from ${romfile}..."; echo ""
    cleanup
    unzip -qj ${romfile} *.apk *build.prop -d ${kdir}/.cache/ROM/system
    cd ${kdir}/.cache/ROM || exit
    devname="$(cat system/build.prop | grep ro.product.device= | cut -d "=" -f2)"; export devname
    echo "Installing device frameworks..."
    apktool if ${kdir}/.cache/ROM/system/framework-res.apk > /dev/null 2>&1
    apktool if ${kdir}/.cache/ROM/system/framework-ext-res.apk > /dev/null 2>&1
    apktool if ${kdir}/.cache/ROM/system/miui.apk > /dev/null 2>&1
    apktool if ${kdir}/.cache/ROM/system/miuisystem.apk > /dev/null 2>&1
    apktool if ${kdir}/.cache/ROM/system/PersonalAssistantPlugin.apk > /dev/null 2>&1
    localelist="-ar -ar-rEG -as-rIN -as-rAZ -az -bg -bg-rBG -bn -bn-rDB -bn-rIN -ca -ca-rES -cs -cs-rCZ -da -da-rDK -de -de-rDE -el -el-rGR -en-rIN -es -es-rES -es-rUS -es-rMX -et-rEE -fa -fa-rIR -fi -fi-rFI -fil -fr -fr-rFR -gu-rIN -ha -he -hi -hi-rIN -hr -hr-rHR -hu -hu-rHU -id -in -in-rID -it -it-rIT
    -iw -iw-rIL -ja -ja-rJP -ka -ka-rGE -kk -kk-rKZ -km -km-rKH -kn-rIN -ko -ko-rKR -lt-rLT -lv -lv-rLV -ml-rIN -mr -ms -ms-rMY -my -my-rMM -nb -nb-rNO -ne-rIN -ne-rNP -or-rIN -pa-rIN -pl -pl-rPL -pt -pt-rBR -pt-rPT -ro -ro-rRO -ru -ru-rRU -sk -sk-rSK -sl -sl-rSI -sr -sr-rRS -sv -sv-rSE -ta -ta-rIN -te -te-rIN -th-rTH -tl -tr -tr-rTR -uk -uk-rUA -ur-rIN -ur-rPK -uz -uz-rUZ -vi -vi-rVN"; export localelist
    for apk in $(cat ${cdir}/devices/${devname}); do
      apk_name="$(basename ${apk})"; export apk_name
      if [[ -e ${kdir}/.cache/ROM/system/${apk} ]]; then
        rm -rf ${kdir}/.cache/apk_wip
        echo ""; echo "Decoding ${apk_name} XMLs..."
        apktool decode -f -s ${kdir}/.cache/ROM/system/${apk} -o ${kdir}/.cache/apk_wip > /dev/null 2>&1
        for xmltype in $(cat ${cdir}/devices/xmltypes); do
          for locale in ${localelist}; do
            if [[ -e ${kdir}/.cache/apk_wip/res/values${locale}/${xmltype}.xml ]]; then
              sed -i '/APKTOOL_DUMMY/d' ${kdir}/.cache/apk_wip/res/values${locale}/${xmltype}.xml
              if [[ ${devname} == ${maindevice} ]]; then
                mkdir -p ${kdir}/XMLs/Global/main/${apk_name}/res/values${locale}
                cp ${kdir}/.cache/apk_wip/res/values${locale}/${xmltype}.xml ${kdir}/XMLs/Global/main/${apk_name}/res/values${locale}
              else
                mkdir -p ${kdir}/XMLs/Global/device/${devname}/${apk_name}/res/values${locale}


                cp ${kdir}/.cache/apk_wip/res/values${locale}/${xmltype}.xml ${kdir}/XMLs/Global/device/${devname}/${apk_name}/res/values${locale}
              fi
            fi
          done
        done
      fi
      cd ${deoinput}/deodexed || exit
    done
  done
}

# APK Pack creator
create_pack() {
  cd ${deoinput}/deodexed || exit
  for romfile in *.zip; do
    echo ""; echo "Creating APK packs for patch testing..."; echo "Current file: ${romfile}"; echo ""
    romdevice="$(echo ${romfile} | cut -d '_' -f2)"
    rombase="$(echo ${romfile} | cut -d "_" -f5 | cut -c1-3)"
    unzip -q ${romfile} -d ${kdir}/.cache/ROM
    cd ${kdir}/.cache/ROM || exit
    if [[ ${rombase} == "6.0" ]] || [[ ${rombase} == "5.1" ]] || [[ ${rombase} == "5.0" ]] || [[ ${rombase} == "4.4" ]]; then
      zip -qr ${romdevice} META-INF/com/google/android/updater-script system/build.prop system/app/miui* system/app/Updater* system/app/ThemeManager* system/app/ThemeModule* system/framework/framework-ext-res* system/framework/framework-res* system/framework/framework.jar system/framework/services.jar system/priv-app/MiuiHome* system/priv-app/Backup* system/priv-app/Browser* system/priv-app/MiuiGallery* system/priv-app/MiuiScanner* system/priv-app/PersonalAssistant* system/priv-app/SecurityCenter* system/priv-app/Settings*
    else
      zip -qr ${romdevice} META-INF/com/google/android/updater-script system/build.prop system/app/miui* system/app/Updater* system/app/ThemeManager* system/app/ThemeModule* system/framework/framework-ext-res* system/framework/framework-res* system/framework/framework.jar system/framework/services.jar system/priv-app/MiuiHome* system/priv-app/Backup* system/priv-app/Browser* system/priv-app/MiuiGallery* system/priv-app/MiuiScanner* system/priv-app/PersonalAssistant* system/priv-app/MiuiKeyguard* system/priv-app/SecurityCenter* system/priv-app/Settings*
    fi
    mv ${romdevice}.zip ${compout}/apkpack/${romdevice}.zip
    cd ${deoinput}/deodexed || exit
    cleanup
  done
  echo "All done. APK packs located in ${compout}/apkpack"
  sleep 2
}

# Repositories management
manage_repos() {
  header; echo "Manage Git repositories"; echo ""
  echo " 1. Publish changes"
  echo " 2. Receive changes"
  echo " b. Voltar"
  footer
  echo -n "Option: "; read -r option
  case ${option} in
    1)
      header; echo "Manage Git repositories"; echo ""
      echo "Sending changes..."
      footer
      cd ${kdir}/repositories || exit
      for repolist in *; do
        cd ${kdir}/repositories/${repolist} || exit
        git add --all
        git commit -m "Repository update $(date +%m/%d/%Y)"
        git push
      done
      echo "Repositories updated."
      sleep 1
    ;;
    2)
      header; echo "Manage Git repositories"; echo ""
      echo "Receiving changes..."
      footer
      cd ${kdir}/repositories || exit
      for repolist in *; do
        cd ${kdir}/repositories/${repolist} || exit
        git pull
      done
      echo "Repositories updated."
      sleep 1
    ;;
  esac
}

# Translation download
download_trans() {
  cd ${kdir}/repositories/${mainXML} || exit
  rm -rf ${mainLOCALE}
  curl https://api.crowdin.com/api/project/${crowdinPROJECT}/export?key=${crowdinAPIKEY}
  curn https://api.crowdin.com/api/project/${crowdinPROJECT}/download/all.zip?key=${crowdinAPIKEY} > translated.zip
  unzip -o translated.zip -d ${mainLOCALE}
  rm -rf translated.zip
  cd ${mainLOCALE} || exit
  cd .. || exit
  git add --all
  git commit -m "Translation update $(date +%m/%d/%Y)"
  git push
}

# ROM Compiler
compile_roms() {
  cd ${deoinput}/deodexed || exit
  sleep 1
  for romfile in *.zip; do
    cleanup
    isglobal="$(echo ${romfile} | grep Global)"; export isglobal
    if [[ ${isglobal} != "" ]]; then
      export basetype="global"
    else
      export basetype="china"
    fi
    header; echo "Compiling ${romfile}..."; footer
    unzip -qj ${romfile} *build.prop -d ${kdir}
    export devname="$(cat ${kdir}/build.prop | grep ro.product.device= | cut -d "=" -f2)"
    rm -rf ${deoinput}/deodexed/build.prop
    romdevice="$(echo ${romfile} | cut -d '_' -f2)"; export romdevice
    romversion="$(echo ${romfile} | cut -d '_' -f3)"; export romversion
    rombase="$(echo ${romfile} | cut -d '_' -f5 | cut -c1-3)"; export rombase
    mv ${deoinput}/deodexed/${romfile} ${cinput}/input.zip
    ${translator} config ${cdir}/data/settings/${basetype}.conf
    mv ${coutput}/*.zip ${compout}/${devname}.zip
    cd ${compout} || exit
    md5ota="$(md5sum ${devname}.zip | cut -d ' ' -f1)"; export md5ota
    rommd5="$(echo ${md5ota} | cut -c1-10)"; export rommd5
    mv ${devname}.zip ${romsite}_${devname}_${romversion}_${rommd5}_${rombase}.zip
    mv ${cinput}/input.zip ${deoinput}/done/${romfile}
    mv ${cdir}/logs/compiler-input.zip.log ${cdir}/logs/${devname}-compiler.log
    mv ${cdir}/logs/decompiler-input.zip.log ${cdir}/logs/${devname}-decompiler.log
    rm -rf ${cdir}/logs/jbart.log ${cdir}/logs/cli.decompiler-input.zip.log
    unset devname
    rm -rf ${kdir}/build.prop
    cd ${deoinput}/deodexed || exit
  done
  echo "All done. Compiled ROMs located in ${compout}"
  sleep 1
}

cleanup; mainmenu
