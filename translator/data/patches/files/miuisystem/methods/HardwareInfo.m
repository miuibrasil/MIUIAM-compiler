.method public static getFreeMemory()J
    .locals 8

    invoke-static {}, Lmiui/util/HardwareInfo;->getFreeMemory_source()J

    move-result-wide v0

    invoke-static {}, Lmiui/util/HardwareInfo;->getTotalPhysicalMemory()J

    move-result-wide v4

    long-to-double v2, v4

    const-wide v6, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v2, v6

    long-to-double v6, v0

    add-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    move-wide v0, v2

    goto :goto_0
.end method

.method public static getFreeMemory_source()J
    .locals 16

    const-wide/16 v14, 0x400

    invoke-static {}, Lmiui/util/HardwareInfo;->getAndroidCacheMemory()J

    move-result-wide v0

    invoke-static {}, Lmiui/util/HardwareInfo;->getTotalPhysicalMemory()J

    move-result-wide v8

    invoke-static {}, Lmiui/util/HardwareInfo;->getTotalMemory()J

    move-result-wide v6

    invoke-static {}, Lmiui/os/MiuiProcessUtil;->getFreeMemory()J

    move-result-wide v10

    div-long v2, v10, v14

    add-long v10, v2, v0

    const-wide/16 v12, 0x2

    mul-long/2addr v12, v8

    sub-long/2addr v12, v6

    mul-long/2addr v10, v12

    div-long/2addr v10, v8

    mul-long v4, v10, v14

    return-wide v4
.end method