#!/bin/bash

cd ${cdir}/projects/input.zip.bzprj/baseROM
echo "Unpacking kernel..."
python3.6 ${kdir}/bin/rmverity.py ${cdir}/projects/input.zip.bzprj/baseROM/boot.img
unpackimg.sh ${cdir}/projects/input.zip.bzprj/baseROM/boot.img >> /dev/null

echo "Patching RAMdisk..."
${usesudo}chown -R $(whoami) ${kerneldir}/ramdisk
${usesudo}chown -R $(whoami) ${kerneldir}/split_img
cd ${kerneldir}/ramdisk || exit
sed -i "s/# secureboot\nservice setlockstate \/sbin\/setlockstate\n    class core\n    oneshot\n    seclabel u:r:vold:s0\n//g" init.miui.rc
rm -rf sbin/setlockstate
for fstab in fstab.*
do
  ${usesudo}sed -i "s/\b,verify\b//g" "${fstab}" 2> /dev/null
  ${usesudo}sed -i "s/fileencryption=ice,quota//g" "${fstab}" 2> /dev/null
  ${usesudo}sed -i "s/fileencryption=ice//g" "${fstab}" 2> /dev/null
  ${usesudo}sed -i "s/forceencrypt=/encryptable=/g" "${fstab}" 2> /dev/null
  ${usesudo}sed -i "s/forcefdeorfbe=/encryptable=/g" "${fstab}" 2> /dev/null
  ${usesudo}sed -i "s/fileencryption/encryptable/g" "${fstab}" 2> /dev/null
  ${usesudo}sed -i "s/ro,barrier/ro,noatime,barrier/g" "${fstab}" 2> /dev/null
done

echo "Patching default.prop..."
for props in default*.prop
do
  ${usesudo}sed -i "/ro.secureboot.devicelock/d" "${props}"
  ${usesudo}sed -i "/ro.bootimage.build.fingerprint/d" "${props}"
  ${usesudo}sed -i "/persist.sys.usb.config/d" "${props}"
  ${usesudo}sed -i "$ a persist.sys.usb.config=mtp,adb" "${props}"
done

echo "Patching boot cmdline..."
cd ${kerneldir}/split_img || exit

export kdevice="$(cat ${cdir}/projects/input.zip.bzprj/baseROM/system/build.prop | grep ro.product.device= | cut -d "=" -f2)"
echo "Patching cmdline for device ${kdevice}"
if [[ ${kdevice} == "ido_xhdpi" ]] ||  [[ ${kdevice} == "natrium" ]] || [[ ${kdevice} == "sagit" ]] || [[ ${kdevice} == "oxygen" ]] || [[ ${kdevice} == "chiron" ]] || [[ ${kdevice} == "meri" ]]; then
  sed -i "s/$/ androidboot.verifiedbootstate=green androidboot.flash.locked=1/" boot.img-cmdline
elif [[ ${kdevice} == "meri" ]] || [[ ${kdevice} == "song" ]]; then
  sed -i "s/$/ androidboot.verifiedbootstate=green androidboot.flash.locked=1 androidboot.veritymode=enforcing/" boot.img-cmdline
else
  sed -i "s/$/ androidboot.verifiedbootstate=green androidboot.bl_state=0 androidboot.flash.locked=1/" boot.img-cmdline
fi

echo "Repacking kernel..."

cd ${cdir} || exit
if [[ ${kdevice} = "meri" ]] || [[ ${kdevice} == "song" ]]; then
  repackunsigned.sh
else
  repackimg.sh
fi

echo "Moving kernel into ROM source..."
mv ${kerneldir}/image-new.img ${cdir}/projects/input.zip.bzprj/baseROM/boot.img

echo "Cleaning up..."
cleanup.sh

echo "Patching build.prop..."
cd ${cdir}/projects/input.zip.bzprj/baseROM/system
rommoddevice=$(cat build.prop | grep ro.product.device= | cut -d "=" -f2)
sed -i "/ro.build.version.incremental=/d" build.prop
sed -i "$ a ro.build.version.incremental=${romversion}" build.prop
if [[ "${rombase}" == "4.4" ]]; then
  sed -i '$ a ro.com.google.gmsversion=4.4_r4' build.prop
elif [[ "${rombase}" == "5.0" ]]; then
  sed -i '$ a ro.com.google.gmsversion=5.0_r5' build.prop
elif [[ "${rombase}" == "5.1" ]]; then
  sed -i '$ a ro.com.google.gmsversion=5.1_r7' build.prop
elif [[ "${rombase}" == "6.0" ]]; then
  sed -i '$ a ro.com.google.gmsversion=6.0_r2' build.prop
elif [[ "${rombase}" == "7.0" ]]; then
  sed -i '$ a ro.com.google.gmsversion=7.0_r3' build.prop
  sed -i '$ a ro.setupwizard.require_network=any' build.prop
elif [[ "${rombase}" == "7.1" ]]; then
  sed -i '$ a ro.com.google.gmsversion=7.1_r3' build.prop
  sed -i '$ a ro.setupwizard.require_network=any' build.prop
elif [[ "${rombase}" == "8.0" ]]; then
  sed -i '$ a ro.com.google.gmsversion=8.0_r3' build.prop
  sed -i '$ a ro.setupwizard.require_network=any' build.prop
fi

export epochdate="$(date +%s)"
export utcdate="$(date -u -d @${epochdate})"
sed -i "$ a ro.build.date=${utcdate}" build.prop
sed -i "$ a ro.build.date.utc=${epochdate}" build.prop

echo "Patching forceencrypt..."
cd ${cdir}/projects/input.zip.bzprj/baseROM/system/vendor/etc
for fstab in fstab*
do
  sed -i "s/fileencryption=ice,quota//g" ${fstab}
done

echo "Fixing Settings.apk..."
cd ${cdir}/projects/input.zip.bzprj/baseROM/system/priv-app/Settings
unzip -q Settings.apk -d unzipped
cd unzipped
zip -0qr Settings.zip *
rm -rf ../Settings.apk
mv Settings.zip ../Settings.apk
cd ..
rm -rf unzipped
