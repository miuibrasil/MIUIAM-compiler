#!/bin/bash

title() {
  clear
  echo "MIUI Brasil ROM Kitchen - $(date +%d/%m/%Y)"
  echo "======================================================================"
  echo ""
}

header() {
  echo "======================================================================"
  echo ""
}

footer() {
  echo ""
  echo "======================================================================"
}

menu(){
  title
  echo "1. Set ROM version (current: ${romversion})"
  echo "2. Download ROMs"
  echo "3. Deodex ROMs"
  echo "4. Extract device Source XMLs"
  echo "5. Extract device Global XMLs"
  echo "6. Create patchable apk zip"
  echo "7. Manage Git repositories"
  echo "8. Download translations"
  echo "C. Compile ROMs"
  echo "U. Upload ROMs"
  echo "X. Exit"
  footer
  echo -n "Option: "
  read -r choice
  case ${choice} in
    1) set_romversion; export choice=""; menu;;
    2) download_roms; export choice=""; menu;;
    3) deodex_roms; export choice=""; cleanup; menu;;
    4) extract_xmls; export choice=""; cleanup; menu;;
    5) extract_gxmls; export choice=""; cleanup; menu;;
    6) fetch_apps; export choice=""; cleanup; menu;;
    7) manage_repos; export choice=""; menu;;
    8) update_translation; export choice=""; menu;;
    C|c) compile_roms; export choice=""; cleanup; menu;;
    U|u) upload_roms; export choice=""; menu;;
    X|x) header; echo "Saindo..."; sleep 1; footer; exit;;
  esac
}

manage_repos(){
  title; echo "Manage Git repositories"; echo ""
  echo "1. Publish changes"
  echo "2. Receive changes"
  echo "b. Back"
  footer
  echo -n "Option: "
  read -r option
  case ${option} in
    1)
      header
      echo ""; echo "Manage Git repositories"; echo ""
      echo "Publishing changes"
      footer
      cd ${kitchendir}/repositories || exit
      for repolist in *; do
        cd ${kitchendir}/repositories/${repolist} || exit
        git add --all
        git commit -m "Repository update ${romversion}"
        git push
      done
      echo "Repositories updated."
      sleep 1
    ;;
    2)
      echo ""; echo "Manage Git repositories"; echo ""
      echo "Receiving changes"
      footer
      cd ${kitchendir}/repositories || exit
      for repolist in *; do
        cd ${kitchendir}/repositories/${repolist} || exit
        git pull
      done
      echo "Repositories updated."
      sleep 1
    ;;
  esac
}

set_romversion() {
  title; echo "Current version: ${romversion}"; footer
  echo -n "Enter ROM version: "
  read -r -e romversion
  ${usesudo}mkdir -p ${soutput}/${romversion}
  ${usesudo}mkdir -p ${routput}/${romversion}
}

download_roms(){
  title
  echo "Download weekly ROMs"
  footer
  cd ${dinput} || exit
  if [[ -e "romlist" ]]; then
    for romurl in $(<romlist); do
      echo ""; echo "Downloading ${romurl}"
      axel -a -n5 ${romurl}
    done
  else
    echo "No ROM links found on the romlist file."
    sleep 2
    cd ${kitchendir} || exit
  fi
}

cleanup(){
  ${usesudo}chown -R "$(whoami)" ${kitchendir}
  ${usesudo}rm -rf ${kitchendir}/.cache/* ${dinput}/thetmpdir
  ${usesudo}rm -rf ${cdir}/projects/* ${cdir}/input/* ${cdir}/output/*
  ${usesudo}rm -rf ${kitchendir}/bin/LODTRTA/source/*
  rm -rf ${rinput}/build.prop
  mkdir -p ${kitchendir}/XMLs ${kitchendir}/.cache/ROM/system ${kitchendir}/.cache/ROM/apk_wip
  ${usesudo}touch ${rinput}/.placeholder ${routput}/.placeholder ${dinput}/.placeholder ${cdir}/input/.placeholder ${cdir}/output/.placeholder ${cdir}/projects/.placeholder
  ${usesudo}touch ${kitchendir}/.cache/.placeholder ${kitchendir}/XMLs/.placeholder ${cdir}/logs/.placeholder ${kitchendir}/.cache/.placeholder ${kitchendir}/XMLs/.placeholder
}

deodex_roms(){
  cd ${dinput} || exit
  for romfile in *.zip; do
    romdevice="$(echo ${romfile} | cut -d '_' -f2)"
    rombase="$(echo ${romfile} | cut -d '_' -f5 | cut -c1-3)"
    cleanup
    if [[ ${rombase} == "8.0" ]]; then
      echo "Deodexing/Converting ${romfile}..."
      convertdex ${dinput}/${romfile}
      mv output.zip ${rinput}/miui_${romdevice}_${romversion}_deodexed_${rombase}.zip
    else
      echo "Converting ${romfile}..."
      convertrom ${dinput}/${romfile}
      mv output.zip ${rinput}/miui_${romdevice}_${romversion}_converted_${rombase}.zip
    fi
      mv ${dinput}/${romfile} ${dinput}/done
    if [[ ${rombase} == "5.1" ]]; then
      echo "Deodexing ${romfile}..."
      unzip -q ${rinput}/miui_${romdevice}_${romversion}_converted_${rombase}.zip -d ${kitchendir}/bin/LODTRTA/source
      xvfb-run java -jar ${kitchendir}/bin/LODTRTA/Launcher.jar ${kitchendir}/bin/LODTRTA/source/system z
      cd ${kitchendir}/bin/LODTRTA/source || exit
      zip -qr output.zip *
      mv output.zip ${rinput}/miui_${romdevice}_${romversion}_deodexed_${rombase}.zip
      rm -rf ${rinput}/miui_${romdevice}_${romversion}_converted_${rombase}.zip
      cd ${dinput} || exit
      rm -rf ${kitchendir}/bin/LODTRTA/source
    fi
    if [[ ${rombase} == "7.0" ]] || [[ ${rombase} == "7.1" ]]; then
      echo "Deodexing ${romfile}..."
      cp ${rinput}/miui_${romdevice}_${romversion}_converted_${rombase}.zip ${cinput}/input.zip
      xvfb-run ${translator} deodex -api 0 -outputdir ${coutput} -romfile ${cinput}/input.zip -workingdir ${cdir}/projects
      if ls ${coutput}/deodexed_input.zip 1> /dev/null 2>&1; then
        mv ${coutput}/deodexed_input.zip ${rinput}/miui_${romdevice}_${romversion}_deodexed_${rombase}.zip
      else
        mv ${cinput}/input.zip ${rinput}/miui_${romdevice}_${romversion}_deodexed_${rombase}.zip
      fi
      rm -rf ${rinput}/miui_${romdevice}_${romversion}_converted_${rombase}.zip
      cd ${dinput} || exit
    fi
  done
  echo "All done. Deodexed ROMs located in ${rinput}."
  echo "Original ROMs moved to ${dinput}/done"
  sleep 2
}

extract_gxmls(){
  cd ${rinput} || exit
  for romfile in *.zip; do
    header; echo "Extracting Global XMLs from ${romfile}..."; footer
    cd ${rinput} || exit
    cleanup
    unzip -qj ${romfile} *.apk *build.prop -d ${kitchendir}/.cache/ROM/system
    cd ${kitchendir}/.cache/ROM || exit
    devicename=$(cat system/build.prop | grep ro.product.device= | cut -d "=" -f2); export devicename
    echo "Installing frameworks..."
    apktool if ${kitchendir}/.cache/ROM/system/framework-res.apk > /dev/null 2>&1
    apktool if ${kitchendir}/.cache/ROM/system/framework-ext-res.apk > /dev/null 2>&1
    apktool if ${kitchendir}/.cache/ROM/system/miui.apk > /dev/null 2>&1
    apktool if ${kitchendir}/.cache/ROM/system/miuisystem.apk > /dev/null 2>&1
    apktool if ${kitchendir}/.cache/ROM/system/PersonalAssistantPlugin.apk > /dev/null 2>&1
    localelist="-ar -ar-rEG -as-rIN -as-rAZ -az -bg -bg-rBG -bn -bn-rDB -bn-rIN -ca -ca-rES -cs -cs-rCZ -da -da-rDK -de -de-rDE -el -el-rGR -en-rIN -es -es-rES -es-rUS -es-rMX -et-rEE -fa -fa-rIR -fi -fi-rFI -fil -fr -fr-rFR -gu-rIN -ha -he -hi -hi-rIN -hr -hr-rHR -hu -hu-rHU -id -in -in-rID -it -it-rIT
    -iw -iw-rIL -ja -ja-rJP -ka -ka-rGE -kk -kk-rKZ -km -km-rKH -kn-rIN -ko -ko-rKR -lt-rLT -lv -lv-rLV -ml-rIN -mr -ms -ms-rMY -my -my-rMM -nb -nb-rNO -ne-rIN -ne-rNP -or-rIN -pa-rIN -pl -pl-rPL -pt -pt-rBR -pt-rPT -ro -ro-rRO -ru -ru-rRU -sk -sk-rSK -sl -sl-rSI -sr -sr-rRS -sv -sv-rSE -ta -ta-rIN -te -te-rIN -th-rTH -tl -tr -tr-rTR -uk -uk-rUA -ur-rIN -ur-rPK -uz -uz-rUZ -vi -vi-rVN"; export localelist
    for apk in $(cat ${kitchendir}/translator/devices/${devicename}); do
      apk_name="$(basename ${apk})"
      export apk_name
      if [[ -e ${kitchendir}/.cache/ROM/system/${apk} ]]; then
        rm -rf ${kitchendir}/.cache/apk_wip
        echo "Decoding ${apk_name}"
        apktool decode -f -s ${kitchendir}/.cache/ROM/system/${apk} -o ${kitchendir}/.cache/apk_wip > /dev/null 2>&1
        for xmltype in $(cat ${kitchendir}/translator/devices/xmltypes); do
          for locale in ${localelist}; do
            if [[ -e ${kitchendir}/.cache/apk_wip/res/values${locale}/${xmltype}.xml ]]; then
              sed -i '/APKTOOL_DUMMY/d' ${kitchendir}/.cache/apk_wip/res/values${locale}/${xmltype}.xml
              if [[ ${devicename} == "${maindevice}" ]]; then
                mkdir -p ${kitchendir}/XMLs/Global/main/${apk_name}/res/values${locale}
                cp ${kitchendir}/.cache/apk_wip/res/values${locale}/${xmltype}.xml ${kitchendir}/XMLs/Global/main/${apk_name}/res/values${locale}/${xmltype}.xml
              else
                mkdir -p ${kitchendir}/XMLs/Global/device/${devicename}/${apk_name}/res/values${locale}
                cp ${kitchendir}/.cache/apk_wip/res/values${locale}/${xmltype}.xml ${kitchendir}/XMLs/Global/device/${devicename}/${apk_name}/res/values${locale}/${xmltype}.xml
              fi
            fi
          done
        done
      fi
    done
  done
cd ${kitchendir} || exit
}

extract_xmls(){
  cd ${rinput} || exit
  for romfile in *.zip; do
    header; echo "Extracting XMLs from ${romfile}..."; footer
    cd ${rinput} || exit
    cleanup
    unzip -qj ${romfile} *.apk *build.prop -d ${kitchendir}/.cache/ROM/system
    cd ${kitchendir}/.cache/ROM || exit
    devicename=$(cat system/build.prop | grep ro.product.device= | cut -d "=" -f2)
    echo "Installing frameworks..."
    apktool if ${kitchendir}/.cache/ROM/system/framework-res.apk > /dev/null 2>&1
    apktool if ${kitchendir}/.cache/ROM/system/framework-ext-res.apk > /dev/null 2>&1
    apktool if ${kitchendir}/.cache/ROM/system/miui.apk > /dev/null 2>&1
    apktool if ${kitchendir}/.cache/ROM/system/miuisystem.apk > /dev/null 2>&1
    apktool if ${kitchendir}/.cache/ROM/system/PersonalAssistantPlugin.apk > /dev/null 2>&1
    for apk in $(cat ${kitchendir}/translator/devices/${devicename}); do
      apk_name="$(basename ${apk})"
      export apk_name
      if [[ -e ${kitchendir}/.cache/ROM/system/${apk} ]]; then
        rm -rf ${kitchendir}/.cache/apk_wip
        echo "Decoding ${apk_name}"
        apktool decode -f -s ${kitchendir}/.cache/ROM/system/${apk} -o ${kitchendir}/.cache/apk_wip > /dev/null 2>&1
        for xmltype in $(cat ${kitchendir}/translator/devices/xmltypes); do
          if [[ -e ${kitchendir}/.cache/apk_wip/res/values${locale}/${xmltype}.xml ]]; then
            sed -i '/APKTOOL_DUMMY/d' ${kitchendir}/.cache/apk_wip/res/values${locale}/${xmltype}.xml
            if [[ ${devicename} == "${maindevice}" ]]; then
              mkdir -p ${kitchendir}/XMLs/Source/main/${apk_name}/res/values
              cp ${kitchendir}/.cache/apk_wip/res/values/${xmltype}.xml ${kitchendir}/XMLs/Source/main/${apk_name}/res/values/${xmltype}.xml
            else
              mkdir -p ${kitchendir}/XMLs/Source/device/${devicename}/${apk_name}/res/values
              cp ${kitchendir}/.cache/apk_wip/res/values/${xmltype}.xml ${kitchendir}/XMLs/Source/device/${devicename}/${apk_name}/res/values/${xmltype}.xml
            fi
          fi
          if [[ -e ${kitchendir}/.cache/apk_wip/res/values-mcc9998-mnc9999/bools.xml ]]; then
            sed -i '/APKTOOL_DUMMY/d' ${kitchendir}/.cache/apk_wip/res/values-mcc9998-mnc9999/bools.xml
            if [[ ${devicename} == "${maindevice}" ]]; then
              mkdir -p ${kitchendir}/XMLs/Source/main/${apk_name}/res/values-mcc9998-mnc9999
              cp ${kitchendir}/.cache/apk_wip/res/values-mcc9998-mnc9999/bools.xml ${kitchendir}/XMLs/Source/main/${apk_name}/res/values-mcc9998-mnc9999/bools.xml
            else
              mkdir -p ${kitchendir}/XMLs/Source/device/${devicename}/${apk_name}/res/values-mcc9998-mnc9999
              cp ${kitchendir}/.cache/apk_wip/res/values-mcc9998-mnc9999/bools.xml ${kitchendir}/XMLs/Source/device/${devicename}/${apk_name}/res/values-mcc9998-mnc9999/bools.xml
            fi
          fi
        done
      fi
    done
  done
cd ${kitchendir} || exit
crowdin upload sources
}

fetch_apps(){
  mkdir -p ${routput}/apkpack
  cd ${rinput} || exit
  for romfile in *.zip; do
    header; echo "Creating APK packs for patch testing..."; echo "ROM atual: ${romfile}"; footer
    romdevice="$(echo ${romfile} | cut -d '_' -f2)"
    rombase="$(echo ${romfile} | cut -d "_" -f5 | cut -c1-3)"
    unzip -q ${romfile} -d ${kitchendir}/.cache/ROM
    cd ${kitchendir}/.cache/ROM || exit
    if [ ${rombase} == "6.0" ] || [ ${rombase} == "5.1" ]; then
      zip -qr ${romdevice} META-INF/com/google/android/updater-script system/build.prop system/app/miui* system/app/FileExplorer* system/app/Updater* system/app/ThemeManager* system/app/ThemeModule* system/framework/framework-ext-res* system/framework/framework-res* system/framework/framework.jar system/framework/services.jar system/priv-app/MiuiHome* system/priv-app/Backup* system/priv-app/Browser* system/priv-app/MiuiGallery* system/priv-app/MiuiScanner* system/priv-app/PersonalAssistant system/priv-app/SecurityCenter* system/priv-app/Settings*
    else
      zip -qr ${romdevice} META-INF/com/google/android/updater-script system/build.prop system/app/miui* system/app/FileExplorer* system/app/Updater* system/app/ThemeManager* system/app/ThemeModule* system/framework/framework-ext-res* system/framework/framework-res* system/framework/framework.jar system/framework/services.jar system/priv-app/MiuiHome* system/priv-app/Backup* system/priv-app/Browser* system/priv-app/MiuiGallery* system/priv-app/MiuiScanner* system/priv-app/PersonalAssistant system/priv-app/MiuiKeyguard* system/priv-app/SecurityCenter* system/priv-app/Settings*
    fi
    mv ${romdevice}.zip ${soutput}/apkpack
    cleanup
    cd ${rinput} || exit
  done
  echo "All done. APK packs located in ${routput}/${romversion}"
  sleep 2
}

compile_roms(){
  cd ${rinput} || exit
  sleep 1
  for romfile in *.zip; do
    cd ${rinput} || exit
    cleanup
    isglobal="$(echo ${romfile} | grep Global)"; export isglobal
    if [[ ${isglobal} != "" ]]; then
      export basetype="global"
    else
      export basetype="china"
    fi
    header; echo "Compiling ${romfile}"; footer
    unzip -qj ${romfile} *build.prop
    devicename=$(cat build.prop | grep ro.product.device= | cut -d "=" -f2); export devicename
    rm -rf build.prop
    romdevice="$(echo ${romfile} | cut -d '_' -f2)"; export romdevice
    rombase="$(echo ${romfile} | cut -d '_' -f5 | cut -c1-3)"; export rombase
    mv ${rinput}/${romfile} ${cinput}/input.zip
    ${translator} config ${cdir}/data/settings/${basetype}.conf
    mv ${coutput}/*.zip ${soutput}/${romsite}_${romtype}_${romdevice}_${romversion}_${rombase}.zip
    cd ${soutput} || exit
    md5ota="$(md5sum ${romsite}_${romtype}_${romdevice}_${romversion}_${rombase}.zip | cut -d ' ' -f1)"; export md5ota
    rommd5="$(echo ${md5ota} | cut -c1-10)"; export rommd5
    mv ${romsite}_${romtype}_${romdevice}_${romversion}_${rombase}.zip ${romversion}/${romsite}_${devicename}_${romversion}_${rommd5}_${rombase}.zip
    mv ${cinput}/input.zip ${rinput}/done/${romfile}
    mv ${cdir}/logs/compiler-input.zip.log ${cdir}/logs/${devicename}-compiler.log
    mv ${cdir}/logs/decompiler-input.zip.log ${cdir}/logs/${devicename}-decompiler.log
    rm -rf ${cdir}/logs/jbart.log ${cdir}/logs/cli.decompiler-input.zip.log
    export devicename=""; export romdevice=""; export rombase=""; export romsize=""; export md5ota=""; export rommd5=""
  done
  echo "All done. Compiled ROMs located in ${routput}."
  sleep 1
}

update_translation(){
  cd ${kitchendir}/repositories/${mainXML} || exit
  rm -rf ${mainLOCALE}
  curl ${crowdinAPI}
  curl ${crowdinDOWNLOAD} > translated.zip
  unzip -o translated.zip -d ${mainLOCALE}
  rm -rf translated.zip
  cd ${mainLOCALE} || exit
  cd ..
  git add --all
  git commit -m "Translation update ${romversion}"
  git push
}

cleanup
menu
